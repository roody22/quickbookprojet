﻿using Intuit.Ipp.OAuth2PlatformClient;
using System;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Net;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.QueryFilter;
using Intuit.Ipp.Security;
using System.Linq;
using System.Collections.Generic;
using MvcCodeFlowClientManual.Model;
using System.Web.UI.WebControls;
using System.Globalization;
using MvcCodeFlowClientManual.Migrations;
using Category = MvcCodeFlowClientManual.Model.Category;

namespace MvcCodeFlowClientManual.Controllers
{
    [Route("App/AppController")]
    public class AppController : Controller
    {
        public static string clientid = ConfigurationManager.AppSettings["clientid"];
        public static string clientsecret = ConfigurationManager.AppSettings["clientsecret"];
        public static string redirectUrl = ConfigurationManager.AppSettings["redirectUrl"];
        public static string environment = ConfigurationManager.AppSettings["appEnvironment"];

        public static OAuth2Client auth2Client = new OAuth2Client(clientid, clientsecret, redirectUrl, environment);
        private DbEntity _db = new DbEntity();

        /// <summary>
        /// Use the Index page of App controller to get all endpoints from discovery url
        /// </summary>
        public ActionResult Index()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            Session.Clear();
            Session.Abandon();
            Request.GetOwinContext().Authentication.SignOut("Cookies");
            return View();
        }

        /// <summary>
        /// Start Auth flow
        /// </summary>
        public ActionResult InitiateAuth(string submitButton)
        {
            switch (submitButton)
            {
                case "Connect to QuickBooks":
                    List<OidcScopes> scopes = new List<OidcScopes>();
                    scopes.Add(OidcScopes.Accounting);
                    string authorizeUrl = auth2Client.GetAuthorizationURL(scopes);
                    return Redirect(authorizeUrl);
                default:
                    return (View());
            }
        }

        /// <summary>
        /// QBO API Request
        /// </summary>
        public ActionResult ApiCallService()
        {
            ProjectData vm = new ProjectData();
            List<Customer> listProject = new List<Customer>();
            if (Session["realmId"] != null)
            {
                string realmId = Session["realmId"].ToString();
                try
                {
                    var principal = User as ClaimsPrincipal;
                    OAuth2RequestValidator oauthValidator = new OAuth2RequestValidator(principal.FindFirst("access_token").Value);

                    // Create a ServiceContext with Auth tokens and realmId
                    ServiceContext serviceContext = new ServiceContext(realmId, IntuitServicesType.QBO, oauthValidator);
                    serviceContext.IppConfiguration.MinorVersion.Qbo = "65";

                    // Create a QuickBooks QueryService using ServiceContext
                    QueryService<Customer> querySvc = new QueryService<Customer>(serviceContext);
                    System.Collections.ObjectModel.ReadOnlyCollection<Customer> customerList = querySvc.ExecuteIdsQuery("SELECT * FROM Customer");

                    foreach (var item in customerList)
                    {
                        if (item.IsProject){
                            listProject.Add(item);
                        }
                    }

                    vm.Project = listProject;
                    vm.Message = "QBO API call Successful!!";
                    return View("ApiCallService", vm);
                }
                catch (Exception ex)
                {
                    vm.Project = listProject;
                    vm.Message = "QBO API call Failed!" + " Error message: " + ex.Message;
                    return View("ApiCallService", vm);
                }
            }
            else {
                vm.Project = listProject;
                vm.Message = "QBO API call Failed!";
                return View("ApiCallService", vm);
            }
        }

        public ActionResult GetSelectedProjectId(FormCollection fc) 
        {
            return RedirectToAction("Dashboard/" + fc["project"]);
        }

        [HttpGet]
        public ActionResult Dashboard(int id)
        {
            string projectId = id.ToString();
            SingleProjectData vm = new SingleProjectData();
            List<Bill> listBill = new List<Bill>();

            if (Session["realmId"] != null)
            {
                string realmId = Session["realmId"].ToString();
                try
                {
                    var dashBoardData = (from c in _db.Contrats
                            join d in _db.Depenses on c.Contract_Id equals d.Contract_Id into ps
                            where c.Project_Id == id
                            from d in ps.DefaultIfEmpty()
                            join cat in _db.Categorys on d.Category_Id equals cat.CategoryId into us
                            from cat in us.DefaultIfEmpty()
                            select new ProjectInformation { Contract_Id = c.Contract_Id,
                                                            ContractAmount = c.Amount,
                                                            DepenseAmount = d == null ? 0 : d.Amount,
                                                            CategoryName = cat.Name == null ? "Aucune" : cat.Name}).ToList();
                    var allContract = (from c in _db.Contrats
                                         where c.Project_Id == id
                                         select new ContratInformation
                                         {
                                             Contract_Id = c.Contract_Id,
                                             ContractAmount = c.Amount,
                                         }).ToList();
                    var dataBudget = (from s in _db.Budgets where s.Project_Id == id select s.Amount == null ? 0 : s.Amount).FirstOrDefault();

                    var listContrat = (from s in _db.Contrats select s).ToList();

                
                        var principal = User as ClaimsPrincipal;
                        OAuth2RequestValidator oauthValidator = new OAuth2RequestValidator(principal.FindFirst("access_token").Value);

                        // Create a ServiceContext with Auth tokens and realmId
                        ServiceContext serviceContext = new ServiceContext(realmId, IntuitServicesType.QBO, oauthValidator);
                        serviceContext.IppConfiguration.MinorVersion.Qbo = "65";

                        QueryService<Customer> querySvc = new QueryService<Customer>(serviceContext);
                        QueryService<Invoice> querySvcInvoice = new QueryService<Invoice>(serviceContext);
                        QueryService<Bill> querySvcBill = new QueryService<Bill>(serviceContext);
                        Customer customer = querySvc.ExecuteIdsQuery("SELECT * FROM Customer where id = '" + projectId + "'").FirstOrDefault();
                        System.Collections.ObjectModel.ReadOnlyCollection<Invoice> invoiceList = querySvcInvoice.ExecuteIdsQuery("select * from Invoice where CustomerRef = '" + projectId + "'");
                        System.Collections.ObjectModel.ReadOnlyCollection<Bill> billList = querySvcBill.ExecuteIdsQuery("select * from Bill");
                        AccountBasedExpenseLineDetail abeld = new AccountBasedExpenseLineDetail();


                    foreach (var bill in billList)
                    {

                        if (bill.Line[0].DetailType.ToString() == "AccountBasedExpenseLineDetail")
                        {
                            abeld = (AccountBasedExpenseLineDetail) bill.Line[0].AnyIntuitObject;
                            // If customerRef exist in the field it's a project related
                            if (abeld.CustomerRef != null && abeld.CustomerRef.Value == projectId)
                            {
                                listBill.Add(bill);
                            }
                            
                        }
 
                    }
                    vm.ListContrat = allContract;
                    vm.Budget = dataBudget;
                    vm.ProjectInformation = dashBoardData;
                    vm.InvoiceList = invoiceList;
                    vm.ProjectBill = listBill;
                    vm.Project = customer;
                    vm.ContratInfo = listContrat;
                    vm.Message = "QBO API call Successful!!";
                    return View("DashBoard", vm);
                }
                catch (Exception ex)
                {
                    vm.Message = "QBO API call Failed!" + " Error message: " + ex.Message;
                    // ToDo Créer une vue d'erreur d'appel API
                    return View("DashBoard", vm);
                }
            }
            else
            {
                vm.Message = "QBO API call Failed!";
                // TODO créer une vue d'erreur d'appel API
                return View("DashBoard", vm);
            }
        }
        [HttpGet]
        public ActionResult ContractDashboard(int id)
        {
            SingleProjectData vm = new SingleProjectData();
            
            if (Session["realmId"] != null)
            {
                try
                {
                    var dashBoardData = (from c in _db.Contrats
                                     where c.Project_Id == id
                                     select new ProjectInformation
                                     {
                                         Contract_Id = c.Contract_Id,
                                         ContractAmount = c.Amount,
                                         ContratName = c.ContratName,
                                         BudgetContratAmount = c.ContratBudget,
                                     }).ToList();
                    vm.ProjectInformation = dashBoardData;
                    vm.Project_Id = id;
                    vm.Message = "QBO API call Successful!!";
                    return View("ContractDashBoard", vm);
                }
                catch (Exception ex)
                {
                    vm.Message = "QBO API call Failed!" + " Error message: " + ex.Message;
                    // ToDo Créer une vue d'erreur d'appel API
                    return View("DashBoard", vm);
                }
            }
            else
            {
                vm.Message = "QBO API call Failed!";
                // TODO créer une vue d'erreur d'appel API
                return View("DashBoard", vm);
            }
        }

        public ActionResult InvoiceDashBoard(FormCollection fc)
        {
            SingleProjectData vm = new SingleProjectData();
            string projectId = fc["project"];

            if (Session["realmId"] != null)
            {
                try
                {
                    string realmId = Session["realmId"].ToString();
                    var principal = User as ClaimsPrincipal;
                    OAuth2RequestValidator oauthValidator = new OAuth2RequestValidator(principal.FindFirst("access_token").Value);

                    // Create a ServiceContext with Auth tokens and realmId
                    ServiceContext serviceContext = new ServiceContext(realmId, IntuitServicesType.QBO, oauthValidator);
                    serviceContext.IppConfiguration.MinorVersion.Qbo = "65";

                    QueryService<Invoice> querySvcInvoice = new QueryService<Invoice>(serviceContext);
                    System.Collections.ObjectModel.ReadOnlyCollection<Invoice> invoiceList = querySvcInvoice.ExecuteIdsQuery("select * from Invoice where CustomerRef = '" + projectId + "'");

                    vm.InvoiceList = invoiceList;
                    vm.Project_Id = Convert.ToInt32(projectId);
                    return View("InvoiceDashboard", vm);
                }
                catch (Exception ex)
                {
                    vm.Message = "QBO API call Failed!" + " Error message: " + ex.Message;
                    // ToDo Créer une vue d'erreur d'appel API
                    return View("DashBoard", vm);
                }
            } else
            {
                vm.Message = "QBO API call Failed!";
                // TODO créer une vue d'erreur d'appel API
                return View("DashBoard", vm);
            }
        }

        public ActionResult BillDashBoard(FormCollection fc)
        {
            SingleProjectData vm = new SingleProjectData();
            string projectId = fc["project"];
            List<Bill> listBill = new List<Bill>();

            if (Session["realmId"] != null)
            {
                try
                {
                    string realmId = Session["realmId"].ToString();
                    var principal = User as ClaimsPrincipal;
                    OAuth2RequestValidator oauthValidator = new OAuth2RequestValidator(principal.FindFirst("access_token").Value);

                    // Create a ServiceContext with Auth tokens and realmId
                    ServiceContext serviceContext = new ServiceContext(realmId, IntuitServicesType.QBO, oauthValidator);
                    serviceContext.IppConfiguration.MinorVersion.Qbo = "65";

                    QueryService<Bill> querySvcBill = new QueryService<Bill>(serviceContext);
                    System.Collections.ObjectModel.ReadOnlyCollection<Bill> billList = querySvcBill.ExecuteIdsQuery("select * from Bill");
                    AccountBasedExpenseLineDetail abeld = new AccountBasedExpenseLineDetail();

                    foreach (var bill in billList)
                    {

                        if (bill.Line[0].DetailType.ToString() == "AccountBasedExpenseLineDetail")
                        {
                            abeld = (AccountBasedExpenseLineDetail)bill.Line[0].AnyIntuitObject;
                            // If customerRef exist in the field it's a project related
                            if (abeld.CustomerRef != null && abeld.CustomerRef.Value == projectId)
                            {
                                listBill.Add(bill);
                            }

                        }

                    }

                    vm.Project_Id = Convert.ToInt32(projectId);
                    vm.ProjectBill = listBill;

                    return View("BillDashboard", vm);
                }
                catch (Exception ex)
                {
                    vm.Message = "QBO API call Failed!" + " Error message: " + ex.Message;
                    // ToDo Créer une vue d'erreur d'appel API
                    return View("DashBoard", vm);
                }
            }
            else
            {
                vm.Message = "QBO API call Failed!";
                // TODO créer une vue d'erreur d'appel API
                return View("DashBoard", vm);
            }
        }


        [HttpGet]
        public ActionResult Budget(int id)
        {
            Project vm = new Project();
            vm.Project_Id = id.ToString();
            return View("AddBudget",vm);
        }

        public ActionResult BudgetUpdate(int id)
        {

            Project vm = new Project();
            var dataBudget = (from s in _db.Budgets where s.Project_Id == id select s.Amount).FirstOrDefault();
            vm.Project_Id = id.ToString();
            vm.BudgetAmount = dataBudget;
            return View("UpdateBudget", vm);
        }

        public ActionResult InsertBudget(FormCollection fc)
        {
            string amount = fc["amount"];
            string projectId = fc["project"];
            BudgetNotQB budget = new BudgetNotQB();
            budget.Amount = Convert.ToDecimal(amount,CultureInfo.InvariantCulture);
            budget.Project_Id = Convert.ToInt32(projectId);
            _db.Budgets.Add(budget);
            _db.SaveChanges();
            return RedirectToAction("Dashboard/" + fc["project"]);
        }

        public ActionResult AddContratBudget(FormCollection fc)
        {
            Project vm = new Project();
            vm.Project_Id = fc["project"];
            vm.Contract_Id = fc["contract"];
            return View("AddContratBudget", vm);
        }

        public ActionResult UpdateContratBudget(FormCollection fc)
        {
            var Contract_Id = Convert.ToInt32(fc["contract"]);
            var Amount = Convert.ToDecimal(fc["amount"], CultureInfo.InvariantCulture);
            var result = (from s in _db.Contrats where s.Contract_Id == Contract_Id select s).FirstOrDefault();
            result.ContratBudget = Amount;
            _db.SaveChanges();
            return RedirectToAction("Dashboard/" + fc["project"]);
        }

        public ActionResult UpdateBudget(FormCollection fc)
        {
            var Project_Id = Convert.ToInt32(fc["project"]);
            var Amount = Convert.ToDecimal(fc["amount"],CultureInfo.InvariantCulture);
            var result = (from s in _db.Budgets where s.Project_Id == Project_Id select s).FirstOrDefault();
            result.Amount = Amount;
            _db.SaveChanges();
            return RedirectToAction("Dashboard/" + fc["project"]);
        }


        [HttpGet]
        public ActionResult AddContract(int id)
        {
            Project vm = new Project();
            vm.Project_Id = id.ToString();
            return View("AddContract",vm);
        }

        //POST
        [HttpPost]
        public ActionResult InsertContract(FormCollection fc)
        {
            Contrat contrat = new Contrat();
            contrat.Amount = Convert.ToDecimal(fc["amount"],CultureInfo.InvariantCulture);
            contrat.Project_Id =Convert.ToInt32( fc["project"]);
            contrat.ContratName = fc["name"];
            _db.Contrats.Add(contrat);
            _db.SaveChanges();
            return RedirectToAction("Dashboard/" + fc["project"]);
        }

        [HttpPost]
        public ActionResult AddDepense(FormCollection fc)
        {
            var category = (from s in _db.Categorys select s).ToList();
            Project vm = new Project();
            vm.Project_Id = fc["project"];
            vm.Contract_Id = fc["contract"];
            vm.CategoryList = category;
            return View("AddDepense", vm);
        }

        [HttpPost]
        public ActionResult SeeContractDepense(FormCollection fc)
        {
            int id = Convert.ToInt32(fc["project"]);
            int contract_id = Convert.ToInt32(fc["contract"]);
            SingleProjectData vm = new SingleProjectData();
            var dashBoardData = (from c in _db.Contrats
                                 join d in _db.Depenses on c.Contract_Id equals d.Contract_Id into ps
                                 where c.Project_Id == id && c.Contract_Id == contract_id
                                 from d in ps.DefaultIfEmpty()
                                 join cat in _db.Categorys on d.Category_Id equals cat.CategoryId into us
                                 from cat in us.DefaultIfEmpty()
                                 select new ProjectInformation
                                 {
                                     Contract_Id = c.Contract_Id,
                                     ContractAmount = c.Amount,
                                     ContratName = c.ContratName,
                                     DepenseAmount = d == null ? 0 : d.Amount,
                                     CategoryName = cat.Name == null ? "Aucune" : cat.Name
                                 }).ToList();
            vm.Project_Id = id;
            vm.ProjectInformation = dashBoardData;
            return View("SeeContractDepense", vm);
        }

        [HttpGet]
        public ActionResult SeeAllDepense(int id)
        {
            SingleProjectData vm = new SingleProjectData();
            var dashBoardData = (from c in _db.Contrats
                                 join d in _db.Depenses on c.Contract_Id equals d.Contract_Id into ps
                                 where c.Project_Id == id
                                 from d in ps.DefaultIfEmpty()
                                 join cat in _db.Categorys on d.Category_Id equals cat.CategoryId into us
                                 from cat in us.DefaultIfEmpty()
                                 select new ProjectInformation
                                 {
                                     Contract_Id = c.Contract_Id,
                                     ContractAmount = c.Amount,
                                     ContratName = c.ContratName,
                                     DepenseAmount = d == null ? 0 : d.Amount,
                                     CategoryName = cat.Name == null ? "Aucune" : cat.Name
                                 }).ToList();
            vm.Project_Id = id;
            vm.ProjectInformation = dashBoardData;
            return View("SeeAllDepense", vm);
        }

        public ActionResult InsertDepense(FormCollection fc)
        {
            string amount = fc["amount"];
            string projectId = fc["project"];
            string contractId = fc["contract"];
            string categoryId = fc["category"];
            Depense depense = new Depense();
            depense.Amount = Convert.ToDecimal(amount, CultureInfo.InvariantCulture);
            depense.Contract_Id = Convert.ToInt32(contractId);
            depense.Category_Id = Convert.ToInt32(categoryId);
            _db.Depenses.Add(depense);
            _db.SaveChanges();
            return RedirectToAction("Dashboard/" + fc["project"]);
        }

        public ActionResult InsertCategory(FormCollection fc)
        {
            string name = fc["name"];
            Category category = new Category();
            category.Name = name;
            Project vm = new Project();
            _db.Categorys.Add(category);
            _db.SaveChanges();
            return AddDepense(fc);
        }
        /// <summary>
        /// Use the Index page of App controller to get all endpoints from discovery url
        /// </summary>
        public ActionResult Error()
        {
            return View("Error");
        }

        

        /// <summary>
        /// Action that takes redirection from Callback URL
        /// </summary>
        public ActionResult Tokens()
        {
            return View("Tokens");
        }
    }
}