﻿namespace MvcCodeFlowClientManual.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contrat",
                c => new
                    {
                        Contract_Id = c.Int(nullable: false, identity: true),
                        Amount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Contract_Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Contrat");
        }
    }
}
