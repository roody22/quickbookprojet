﻿namespace MvcCodeFlowClientManual.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BudgetTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Budget",
                c => new
                    {
                        Budget_Id = c.Int(nullable: false, identity: true),
                        Amount = c.Int(nullable: false),
                        Project_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Budget_Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Budget");
        }
    }
}
