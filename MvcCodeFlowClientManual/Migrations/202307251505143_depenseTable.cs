﻿namespace MvcCodeFlowClientManual.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class depenseTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Depense",
                c => new
                    {
                        Depense_Id = c.Int(nullable: false, identity: true),
                        Contract_Id = c.Int(nullable: false),
                        Amount = c.Int(nullable: false),
                        Project_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Depense_Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Depense");
        }
    }
}
