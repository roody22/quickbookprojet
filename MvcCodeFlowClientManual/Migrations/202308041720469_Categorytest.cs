﻿namespace MvcCodeFlowClientManual.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Category : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Category",
                c => new
                    {
                        CategoryId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        DepenseId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CategoryId);
            
            AlterColumn("dbo.Contrat", "Amount", c => c.Single(nullable: false));
            AlterColumn("dbo.Depense", "Amount", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Depense", "Amount", c => c.Int(nullable: false));
            AlterColumn("dbo.Contrat", "Amount", c => c.Int(nullable: false));
            DropTable("dbo.Category");
        }
    }
}
