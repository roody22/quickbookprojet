﻿namespace MvcCodeFlowClientManual.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CategoryChange : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Category", "DepenseId", c => c.Int());
            AlterColumn("dbo.Contrat", "Amount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Depense", "Amount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.Depense", "Project_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Depense", "Project_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Depense", "Amount", c => c.Single(nullable: false));
            AlterColumn("dbo.Contrat", "Amount", c => c.Single(nullable: false));
            AlterColumn("dbo.Category", "DepenseId", c => c.Int(nullable: false));
        }
    }
}
