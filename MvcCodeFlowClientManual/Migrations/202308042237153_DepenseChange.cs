﻿namespace MvcCodeFlowClientManual.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DepenseChange : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Depense", "Category_Id", c => c.Int(nullable: false));
            DropColumn("dbo.Category", "DepenseId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Category", "DepenseId", c => c.Int());
            DropColumn("dbo.Depense", "Category_Id");
        }
    }
}
