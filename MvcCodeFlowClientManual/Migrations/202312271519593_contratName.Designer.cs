﻿// <auto-generated />
namespace MvcCodeFlowClientManual.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class contratName : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(contratName));
        
        string IMigrationMetadata.Id
        {
            get { return "202312271519593_contratName"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
