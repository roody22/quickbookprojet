﻿namespace MvcCodeFlowClientManual.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class contratName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contrat", "ContratName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Contrat", "ContratName");
        }
    }
}
