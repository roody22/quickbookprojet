﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcCodeFlowClientManual.Model
{
    public class BudgetNotQB
    {
        [Key, Column(Order = 1)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Budget_Id { get; set; }
        public decimal Amount { get; set; }
        public int Project_Id { get; set; }
    }
}