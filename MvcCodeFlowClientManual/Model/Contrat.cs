﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MvcCodeFlowClientManual.Model
{
    public class Contrat
    {
        [Key, Column(Order = 1)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Contract_Id { get; set; }
        public decimal Amount { get; set; }

        public decimal ContratBudget { get; set; }
        public int Project_Id { get; set; }
        public string ContratName { get; set;}

    }
}