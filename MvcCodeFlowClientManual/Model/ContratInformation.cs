﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcCodeFlowClientManual.Model
{
    public class ContratInformation
    {
        public int Contract_Id { get; set; }
        public decimal ContractAmount { get; set; }
    }
}