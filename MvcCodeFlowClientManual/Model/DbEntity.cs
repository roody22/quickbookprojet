﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace MvcCodeFlowClientManual.Model
{
    public class DbEntity : DbContext
    {
        public DbEntity() : base("demoASPEntities")
        {
        }
        public DbSet<Contrat> Contrats { get; set; }

        public DbSet<BudgetNotQB> Budgets { get; set; }

        public DbSet<Depense> Depenses { get; set; }

        public DbSet<Category> Categorys { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Contrat>().ToTable("Contrat");
            modelBuilder.Entity<BudgetNotQB>().ToTable("Budget");
            modelBuilder.Entity<Category>().ToTable("Category");
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }
    }
}