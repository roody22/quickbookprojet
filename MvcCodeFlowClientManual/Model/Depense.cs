﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcCodeFlowClientManual.Model
{
    public class Depense
    {
        [Key, Column(Order = 1)]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Depense_Id { get; set; }
        public int Contract_Id { get; set; }
        public int Category_Id { get; set; }
        public decimal Amount { get; set; }

        // To do primaryKey composite

    }
}