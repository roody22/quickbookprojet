﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcCodeFlowClientManual.Model
{
    public class Project
    {
        public string Project_Id { get; set; }
        public string Contract_Id { get; set; }
        public decimal BudgetAmount { get; set; }
        public List<Category> CategoryList { get; set; }
}
}