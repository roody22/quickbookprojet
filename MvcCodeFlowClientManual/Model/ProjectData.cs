﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Intuit.Ipp.Data;

namespace MvcCodeFlowClientManual.Model
{
    public class ProjectData
    {
        public List<Customer> Project { get; set; }
        public string Message { get; set; }
    }
}