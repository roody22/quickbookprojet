﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcCodeFlowClientManual.Model
{
    public class ProjectInformation
    {
        public int Contract_Id { get; set; }
        public decimal ContractAmount { get; set; }

        public decimal BudgetContratAmount { get; set; }
        public decimal DepenseAmount { get; set; }
        public string CategoryName { get; set; }
        public string ContratName { get; set; }
    }
}