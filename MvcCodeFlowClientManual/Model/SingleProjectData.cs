﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Intuit.Ipp.Data;

namespace MvcCodeFlowClientManual.Model
{
    public class SingleProjectData
    {
       public Customer Project { get; set; }
       public string Message { get; set; }

       public System.Collections.ObjectModel.ReadOnlyCollection<Invoice> InvoiceList { get; set; }

       public List<Bill> ProjectBill { get; set; }

       public List<ContratInformation> ListContrat { get; set; }

        public List<ProjectInformation> ProjectInformation { get; set; }

       public Decimal Budget { get; set; }

       public List<Contrat> ContratInfo { get; set; }

        public int Project_Id { get; set; }

    }
}