# QuickBookProjet

## Name
Quickbook project information

## Description

This project is for company who strugle to have good information about their project in QuickBook.
The project use Auth2 identification with QuickBook login.  The most difficult part for now was
to find a way to get project related information because quickBook don't give the project_Id on a single 
Querry.  I have to search hard on documentation to find a way to get this information.


## Getting started

Create a QuickBook Dev acount

put a project on the sandbox give by QuickBook

To run the project on visual studio, click on MVCManualCodeFlow.sln. Build the project and run it.

Create Db (outil / gestionnaire de pakage nuget / console nuget)
comande in nuget package terminal
Update-Database

## Tools

This project use Entity Frameworks to build the dadtabase.
to Update Db migration (Go to outil / gestionnaire de pakage nuget / console nuget)
comande in nuget package terminal
Add-Migration nameOfMigration
Update-Database

## Installation
Everything to install is on the nuget package

## Support
No support is offer, for the moment.

## Roadmap
No date for release for now.

## Contributing
No contribution authorize for the moment.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
Check the license file

## Project status
The project is for learning .net with a real use case project.
